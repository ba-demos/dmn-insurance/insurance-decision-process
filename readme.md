Insurance Decision Process
==========================

Simple process that call a DMN decision.
Whether the result of the decision is more 1500, engage a human task

![process](src/main/resources/InsuranceDecisionProcess.InsuranceQuote-svg.svg)